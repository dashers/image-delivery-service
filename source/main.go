package main

import (
	"fmt"
	"net/http"
)

func main() {
	ImageDeliveryService()
	fmt.Println("Server is listening on port: :9000")
	http.ListenAndServe(":9000", nil)
}
