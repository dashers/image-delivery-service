package main

import (
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"net/http"
	"os"
	"strconv"

	"github.com/muesli/smartcrop"
	"github.com/muesli/smartcrop/nfnt"
	"github.com/nfnt/resize"
)

// Image that will be saved to local machine
type LocalImage struct {
	Image image.Image

	Type string
}

// Gets image from given path <IMG_SOURCE_URL> - if it's not an image or image nor found then returns nothing.
// If image fullfills conditions (image type), then crops/resizes the image - if not crop/resize is skipped - and saves the image.
// Returns location path of the image in file system
func ProcessImage(imageInfo ParsedImageRequest) (string, *RequestError) {

	img, err := downloadFile(os.Getenv("IMG_SOURCE_URL") + imageInfo.imagePath)

	if err != nil {
		return "", err
	}

	// @TODO add image/png
	if img.Type == "image/jpeg" || img.Type == "image/png" {
		var imgWidth uint
		imgWidth = 0
		if imageInfo.width != "x" {
			n, err := strconv.Atoi(imageInfo.width)
			if err == nil {
				imgWidth = uint(n)
			}
		}

		var imgHeight uint
		imgHeight = 0
		if imageInfo.height != "x" {
			n, err := strconv.Atoi(imageInfo.height)
			if err == nil {
				imgHeight = uint(n)
			}
		}

		// if width and height are strongly defined, then find best crop using analyzer
		if imgHeight > 0 && imgWidth > 0 {
			img.Image = makeBestCrop(img.Image, int(imgWidth), int(imgHeight))
		}

		// resize image
		img.Image = resize.Resize(imgWidth, imgHeight, img.Image, resize.Lanczos3)

		saveImageError := saveImageToFile(img, imageInfo)
		if saveImageError != nil {
			return "", saveImageError
		}
	}

	saveImageError := saveImageToFile(img, imageInfo)
	if saveImageError != nil {
		return "", saveImageError
	}

	return imageInfo.localFilePath, nil
}

func makeBestCrop(img image.Image, width int, height int) image.Image {
	analyzer := smartcrop.NewAnalyzer(nfnt.NewDefaultResizer())
	topCrop, _ := analyzer.FindBestCrop(img, width, height)

	type SubImager interface {
		SubImage(r image.Rectangle) image.Image
	}
	return img.(SubImager).SubImage(topCrop)
}

func downloadFile(URL string) (*LocalImage, *RequestError) {
	//Get the response bytes from the url
	response, err := http.Get(URL)
	if err != nil {
		fmt.Println(err)
		return nil, &RequestError{
			StatusCode: 500,
			Err:        errors.New("Could not get original image: " + URL),
		}
	}
	defer response.Body.Close()

	if response.StatusCode == 404 {
		return nil, &RequestError{
			StatusCode: response.StatusCode,
			Err:        errors.New("Original image not found"),
		}
	}

	contentType := response.Header.Get("Content-Type")
	image, _, err := image.Decode(response.Body)
	if err != nil {
		return nil, &RequestError{
			StatusCode: 500,
			Err:        err,
		}
	}

	return &LocalImage{
		Image: image,
		Type:  contentType,
	}, nil
}

// creates file on local machine with needed structure and writes image to this file
func saveImageToFile(img *LocalImage, imageInfo ParsedImageRequest) *RequestError {
	// prepare directory structure if not exist
	os.MkdirAll(imageInfo.directoryStructure, os.ModeDir)
	outFile, fileCreationError := os.Create(imageInfo.localFilePath)
	if fileCreationError != nil {
		fmt.Println(fileCreationError)
		return &RequestError{
			StatusCode: 500,
			Err:        fileCreationError,
		}
	}
	defer outFile.Close()

	// then save to file
	// @TODO distinguish image/png files
	if img.Type == "image/jpeg" {
		jpegErr := jpeg.Encode(outFile, img.Image, nil)
		if jpegErr != nil {
			return &RequestError{
				StatusCode: 500,
				Err:        jpegErr,
			}
		}
	} else if img.Type == "image/png" {
		pngErr := png.Encode(outFile, img.Image)
		if pngErr != nil {
			return &RequestError{
				StatusCode: 500,
				Err:        pngErr,
			}
		}
	} else if img.Type == "image/gif" {
		gifErr := gif.Encode(outFile, img.Image, nil)
		if gifErr != nil {
			return &RequestError{
				StatusCode: 500,
				Err:        gifErr,
			}
		}
	} else {
		return &RequestError{
			StatusCode: 415,
			Err:        errors.New("Unsupported image type"),
		}
	}

	return nil
}
