package main

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"regexp"
	"strings"
)

type ParsedImageRequest struct {
	width              string
	height             string
	imagePath          string
	localFilePath      string
	directoryStructure string
}

func ImageDeliveryService() {

	imageSizes := strings.Split(os.Getenv("IMG_SIZES"), ",")

	for _, size := range imageSizes {
		http.HandleFunc("/"+size+"/", func(res http.ResponseWriter, req *http.Request) {
			parsedImageRequest := parseImageRequest(req.RequestURI)
			//check if file exists
			f, err := os.Open(parsedImageRequest.localFilePath)
			defer f.Close()

			if err != nil {

				// if image does not exist, download it, crop, save and return as response
				if os.IsNotExist(err) {

					processedImageFilePath, err := ProcessImage(parsedImageRequest)
					if err != nil {
						fmt.Println(err)
						res.WriteHeader(err.StatusCode)
						res.Write([]byte("Error occured 😢\n" + err.Err.Error()))
						return
					}
					http.ServeFile(res, req, processedImageFilePath)
					return
				}
			}
			http.ServeFile(res, req, parsedImageRequest.localFilePath)
		})
	}
}

func parseImageRequest(requestURI string) ParsedImageRequest {
	re := regexp.MustCompile(`\/([0-9|x]*):([0-9|x]*)\/(.*)`)
	matches := re.FindAllStringSubmatch(requestURI, -1)

	if len(matches[0]) < 4 {
		return ParsedImageRequest{}
	}
	width := matches[0][1]
	height := matches[0][2]
	imagePath := matches[0][3]

	//path to file with file
	localFilePath := path.Join("./static", width+":"+height, imagePath)
	fmt.Println(localFilePath)

	// path to file without the file name
	imageLocalFilePathArray := strings.Split(localFilePath, "/")
	imageLocalFilePathArray = imageLocalFilePathArray[:len(imageLocalFilePathArray)-1]
	directoryStructure := ""
	for _, dir := range imageLocalFilePathArray {
		directoryStructure += dir + "/"
	}
	return ParsedImageRequest{width, height, imagePath, localFilePath, directoryStructure}
}
