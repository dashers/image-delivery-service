module gitlab.com/dashers/image-delivery-service

go 1.15

require (
	github.com/muesli/smartcrop v0.3.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
)
