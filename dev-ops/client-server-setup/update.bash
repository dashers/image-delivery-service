#!/usr/bin/env bash
set -e;

# extract script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Deploy config
CI_REGISTRY_USER=""
CI_REGISTRY_PASSWORD=""
CI_REGISTRY="registry.gitlab.com"


########################################################################################################################
# HELPER CONFIGURATION FUNCTION
########################################################################################################################
docker-compose-configured() {
    docker-compose \
        --project-name "image-delivery-service"
        --env-file ${DIR}/.env \
        --file=${DIR}/docker-compose.yml \
        $@
}

########################################################################################################################
# DEPLOY STARTED HERE
########################################################################################################################
# Ask the user for their name
echo "Updating application started (write image version you want to deploy for example: '1.0.0'):"
read IDS_TAG

echo "You will deploy image-delivery-service version: '${IDS_TAG}', it is correct? [y]"
read CORRECT

if [ "$CORRECT" != "y" ]; then
    echo "Command aborted. You can try again"
    exit;
fi;

# export docker-compose variables
export IDS_TAG=${IDS_TAG}

# START UPDATE
echo "Logging into registry..."
echo -n $CI_REGISTRY_PASSWORD | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY

echo "Cleaning up old app version..."
docker-compose-configured down --volumes

echo "Starting new app version...."
docker-compose-configured up --detach

echo "Logging out from registry..."
docker logout

echo ""
echo "############################################################################"
echo "# Congratulation! Application was successfully updated to version ${IDS_TAG}"
echo "############################################################################"
echo ""