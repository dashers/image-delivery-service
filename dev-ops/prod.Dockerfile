FROM golang:1.15-alpine3.12

RUN apk add git

WORKDIR /go/src/gitlab.com/dashers/image-delivery-service

ENV GO111MODULE=on
ADD ./go.mod .
ADD ./go.sum .

# pull in any dependencies
RUN go mod download

ADD . .

## Our project will now successfully build with the necessary go libraries included.
RUN go build -o main .

## Our start command which kicks off
## our newly created binary executable

EXPOSE 9000

CMD ["./main"]