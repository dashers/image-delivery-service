FROM golang:1.15-alpine3.12

RUN mkdir /app \
    && apk add git

WORKDIR /app

# get and install watcher
RUN go get github.com/canthefason/go-watcher \
    && go install github.com/canthefason/go-watcher/cmd/watcher

ENV GO111MODULE=on
ADD ./source/go.mod /app
ADD ./source/go.sum /app

# pull in any dependencies
RUN go mod download

ADD ./source /app

EXPOSE 9000

CMD ["watcher"]